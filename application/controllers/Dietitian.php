<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Dietitian extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('dietitian_model');
		$this->load->model('patient_model');
        $this->load->model('recent_visitor_model');
        $this->load->model('booking_model');


		// $this->output->enable_profiler(TRUE);  // Just for debugging stuffs
	}

	public function index() {
		$data['title'] = "login";

		$this->load->view('home', $data);
		$this->load->view('templates/footer');
		
		$username=$this->input->post('identity');
		$password=$this->input->post('password');
		
		if ($username=="123@123.com" && $password=="password")
		{
			redirect('/dietitian/homepage', 'refresh');
		}
	}

	public function account_settings() {

		$data['title'] = "Deititian's account settings";

		$this->load->view('templates/header', $data);
		$this->load->view('account_settings');
		$this->load->view('templates/footer');
		
	}

	public function help() {

		$data['title'] = "Help page";

		$this->load->view('templates/header', $data);
		$this->load->view('help');
		$this->load->view('templates/footer');
		
	}


	public function homepage() {
		$data['template']= $this->patient_model->get_insert_template();
		$data['title'] = "Dietitian's Homepage";

		$this->load->view('templates/header', $data);
		$this->load->view('homepage', $data);
		$this->load->view('templates/footer');

	}

	function calendar(){
        $name['title'] = "calendar";

        $this->load->view('templates/header', $name);
        $this->load->view('calendar');

	}

	function get_event() {


		$r = $this->booking_model->get_events();
		echo json_encode($r);


    }


	public function education() {

		$data['title'] = "education";
		$this->load->view('templates/header', $data);
		$this->load->view('education', $data);
		$this->load->view('templates/footer');

	}


	public function recentvist() {

		$data['title'] = "recent visters";
		$this->load->view('templates/header', $data);
        $recent['recent']= $this->recent_visitor_model->get_recent_visitors();

		$this->load->view('recent_visitor', $recent);
		$this->load->view('templates/footer');

	}

// ---------------------------------------------------------------------

//						Helper Functions Below

// ---------------------------------------------------------------------



	public function create_patient() {

		$data = array(
			'users_id' => $this->input->post('users_id'),
			'patient_first_name' => $this->input->post('patient_first_name'),
			'patient_patient_last_name' => $this->input->post('patient_last_name'),
			'patient_gender' => $this->input->post('patient_gender'),
			'patient_dob' => $this->input->post('patient_dob'),
			'patient_email' => $this->input->post('patient_email'),
		);

		$insert = $this->patient_model->add_patient($data);
		
		echo json_encode(array("status" => TRUE));

	}

	public function search() {

		$search_data = $this->input->post('search_data');

		// echo json_encode($search_data);

		$res = $this->patient_model->get_patient($search_data);

		if (!empty($res)) { // if $res is not empty

			foreach ($res as $row) {
				$link = base_url() . "patient" . "/" . $row->id;
				echo "<li><a href='$link'>" . $row->patient_first_name . " " . $row->patient_patient_last_name . "  -  " . $row->patient_email . "</a></li>";
			}

		}

		else {
			echo "<li> <em> Patient not in database <em></li>";
		}
		
	}



}

?>

