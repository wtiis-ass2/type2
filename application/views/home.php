<html>
<head>
  
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.0/flatly/bootstrap.min.css" rel="stylesheet" integrity="sha384-kCsv8pSAWtRge/+zcLDeqwoWhTQSUX2esQPYOsocgrg1eMj7T2wrTJP348T3mpBU" crossorigin="anonymous">

  <title><?= $title ?></title>

</head>
<body>
<div class="container h-100">
  <div class="row h-100 justify-content-center align-items-center">
   <form class="form-signin" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Type 2 System</h1>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="identity" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">

        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">

        
        <div class="checkbox mb-3">
          <label>
             <input type="checkbox" name="remember" value="1" id="remember" />
             <label for="remember"> Remember Me</label>    
            <!-- <input type="checkbox" value="remember-me"> Remember me -->
          </label>
        </div>

        <p><input type="submit" class="btn btn-lg btn-primary btn-block" name="submit" value="Login"  /></p>

        <!-- <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button> -->

        <!-- <p class="mt-5 mb-3 text-muted">© 2017-2018</p> -->
        <!-- </form> -->
        <!-- <button type="submit" class="btn btn-primary">Sign in</button> -->
      </form>
  </div>
</div>