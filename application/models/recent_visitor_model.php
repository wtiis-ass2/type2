<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//This is the Book Model for CodeIgniter CRUD using Ajax Application.


class recent_visitor_model extends CI_Model {

    public $table_name='recent10';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_recent_visitors() {

        $this->db->select("id, first_name, last_name, last_vist_date");
        $query = $this->db->get($this->table_name);
        return $query->result_array();	// This returns an array of rows

    }
}

?>
