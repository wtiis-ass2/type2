<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//This is the Book Model for CodeIgniter CRUD using Ajax Application.


class dietitian_model extends CI_Model {

	public $table_name='users';

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_all_users() {

		$this->db->from($this->table_name);  
		$query = $this->db->get();

		return $query->result();	// This returns an array of rows

	}

}

?>
