<div class="mt-5">
    <div class="list-group">

        <div class="list-group-item list-group-item-action">
            <h4>Type 2 Diabetes</h4>
            <p>Type 2 diabetes is a progressive condition in which the body becomes resistant to the normal effects of insulin and/or gradually loses the capacity to produce enough insulin in the pancreas. We do not know what causes type 2 diabetes. Type 2 diabetes is associated with modifiable lifestyle risk factors. Type 2 diabetes also has strong genetic and family related risk factors.</p>
            <a href="https://www.diabetesaustralia.com.au/type-2-diabetes" target="_blank" class="card-link">Learn More</a>
        </div>

        <div class="list-group-item list-group-item-action">
            <h4>What happens with type 2 diabetes?</h4>
            <p>Type 2 diabetes develops over a long period of time (years). During this period of time insulin resistance starts, this is where the insulin is increasingly ineffective at managing the blood glucose levels. As a result of this insulin resistance, the pancreas responds by producing greater and greater amounts of insulin, to try and achieve some degree of management of the blood glucose levels.</p>
            <a href="https://www.diabetesaustralia.com.au/type-2-diabetes" target="_blank" class="card-link">Learn More</a>
        </div>
        <div class="list-group-item list-group-item-action">
            <h4>What causes type 2 diabetes?</h4>
            <p>Diabetes runs in the family. If you have a family member with diabetes, you have a genetic disposition to the condition.

                While people may have a strong genetic disposition towards type 2 diabetes, the risk is greatly increased if people display a number of modifiable lifestyle factors including high blood pressure, overweight or obesity, insufficient physical activity, poor diet and the classic ‘apple shape’ body where extra weight is carried around the waist.</p>
            <a href="https://www.diabetesaustralia.com.au/type-2-diabetes" target="_blank" class="card-link">Learn More</a>
        </div>
    </div>


</div>




