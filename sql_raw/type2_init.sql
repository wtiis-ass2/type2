-- create database
DROP DATABASE IF EXISTS type2;
CREATE DATABASE type2;


-- drop tables if already exists
DROP TABLE IF EXISTS type2.booking;
DROP TABLE IF EXISTS type2.patient_record;
DROP TABLE IF EXISTS type2.patient;
DROP TABLE IF EXISTS type2.dietitian;
DROP TABLE IF EXISTS type2.time_slot;
DROP TABLE IF EXISTS type2.admin;


-- create tables

CREATE TABLE type2.admin (
    admin_id int NOT NULL AUTO_INCREMENT,
    admin_name varchar(255) NOT NULL,
    admin_email varchar(255),
    admin_password varchar(255) NOT NULL,
    PRIMARY KEY (admin_id)
);


CREATE TABLE type2.time_slot (
    slot_id int NOT NULL AUTO_INCREMENT,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    PRIMARY KEY (slot_id)
);

CREATE TABLE type2.dietitian (
    dietitian_id int NOT NULL AUTO_INCREMENT,
    dietitian_first_name varchar(255),
    dietitian_last_name varchar(255),
	dietitian_password varchar(255) NOT NULL,
	dietitian_email varchar(255),
    PRIMARY KEY (dietitian_id)
);

CREATE TABLE type2.patient (
    patient_id int NOT NULL AUTO_INCREMENT,
    dietitian_id int,
    patient_first_name varchar(255),
    patient_last_name varchar(255),
    patient_gender ENUM('F','M','neuter') NOT NULL,
    patient_dob DATE NOT NULL,
    patient_email varchar(255),
    PRIMARY KEY (patient_id),
    FOREIGN KEY (dietitian_id) REFERENCES dietitian(dietitian_id)
);

CREATE TABLE type2.patient_record (
    record_id int NOT NULL AUTO_INCREMENT,
    patient_id int,
    record_created_date DATE NOT NULL,
    record_modified_date DATE NOT NULL,
	blood_pressure decimal,
	blood_glucose_level decimal,
	waist_circumfrerence decimal,
	weight decimal,
	height decimal,
	validity_flag bit NOT NULL,
    PRIMARY KEY (record_id),
    FOREIGN KEY (patient_id) REFERENCES patient(patient_id)
);


CREATE TABLE type2.booking (
    booking_id int NOT NULL AUTO_INCREMENT,
    dietitian_id int,
    patient_id int,
    slot_id int,
    booking_created_date DATE NOT NULL,
    booking_modified_date DATE NOT NULL,
	booking_date DATE NOT NULL,
	validity_flag bit NOT NULL,
    PRIMARY KEY (booking_id),
    FOREIGN KEY (dietitian_id) REFERENCES dietitian(dietitian_id),
    FOREIGN KEY (patient_id) REFERENCES patient(patient_id),
    FOREIGN KEY (slot_id) REFERENCES time_slot(slot_id)
);




INSERT INTO type2.time_slot (start_time, end_time)
VALUES
("08:00", "10:00"),
("10:00", "12:00"),
("12:00", "14:00"),
("14:00", "16:00");


INSERT INTO type2.dietitian (dietitian_first_name, dietitian_last_name,dietitian_password,dietitian_email)
VALUES 
("Lizzie","Gilliam","password","LizzieGGilliam@teleworm.us"),
("Virginia","Evans","password","VirginiaJEvans@armyspy.com"),
("Clay","Ferris","password","ClayNFerris@teleworm.us"),
("Elwood","Caulfield","password","ElwoodECaulfield@rhyta.com"),
("Denise","Sanford","password","DeniseJSanford@teleworm.us");



-- INSERT INTO type2.patient VALUES
-- (32000,2007,"Tammy","Fox","F","1971-07-23","TammyPFox@armyspy.com");


INSERT INTO type2.patient (dietitian_id,patient_first_name, patient_last_name,patient_gender,patient_dob,patient_email)
VALUES 
(1,"Tammy","Fox","F","1971-07-23","TammyPFox@armyspy.com"),
(2,"Terry","Chan","F","1990-05-25","TerryCChan@dayrep.com"),
(3,"Javier","Martinez","M","1969-09-17","JavierJMartinez@teleworm.us"),
(4,"Alicia","Hiller","F","1932-06-19","AliciaGHiller@jourrapide.com"),
(5,"Douglas","Norris","M","1985-11-14","DouglasMNorris@teleworm.us"),
(4,"Jean","Conner","F","1985-03-02","JeanEConner@dayrep.com"),
(3,"Mildred","Langston","F","1978-12-02","MildredOLangston@jourrapide.com"),
(4,"Christina","Yates","F","1970-05-30","ChristinaJYates@dayrep.com"),
(2,"Ericka","Kline","F","1990-04-25","ErickaVKline@jourrapide.com");


INSERT INTO type2.patient_record (patient_id, record_created_date,record_modified_date,blood_pressure,blood_glucose_level,waist_circumfrerence,weight,height,validity_flag)
VALUES 
(9,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(2,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(1,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(3,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(4,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(5,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(6,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(7,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(8,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(5,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(6,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(8,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(4,"2018-04-23","2018-04-23",150,231,123,150,150,0),
(2,"2018-04-23","2018-04-23",150,231,123,150,150,0);


INSERT INTO type2.booking (dietitian_id, patient_id,slot_id,booking_created_date,booking_modified_date, booking_date,validity_flag)
VALUES 
(3,5,1,"2018-04-13","2018-04-13","2018-05-03",0),
(1,8,3,"2018-04-23","2018-04-23","2018-05-23",0),
(4,4,4,"2018-04-23","2018-04-23","2018-06-13",0);
