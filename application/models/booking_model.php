<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//This is the Book Model for CodeIgniter CRUD using Ajax Application.


class booking_model extends CI_Model {


    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_events(){
        $this->db->select("id, title, start, end");
        $this->db->from('booking_event');
        return $this->db->get()->result();
    }

//    public function get_events() {
//
//        $this->db->select("title, id, start, end");
//        $query = $this->db->get($this->table_name);
//
//        return $query->result();
//
////        return $this->db->get($this->table_name);
//
//    }
}

?>
