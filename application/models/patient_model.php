<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//This is the Book Model for CodeIgniter CRUD using Ajax Application.


class patient_model extends CI_Model {

	public $table_name='patient';

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_insert_template() {

		$result = $this->db->list_fields($this->table_name);

		foreach ($result as $item) {

			$pretty = str_replace('_', ' ', ucfirst($item));
			// $awesome = str_replace('_', ' ', ucfirst($item))
			$out[] = $pretty; 	// appending item into the $out array 

		}

		// $final = array_shift($out);
		unset($out[0]);
		return $out;	// This returns an array of rows
	}

	public function add_patient($data) {
		
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();

	}

	public function get_patient($search_data) {

		$this->db->select('id, patient_first_name, patient_patient_last_name, patient_email');
		$this->db->like('patient_first_name', $search_data);
		$this->db->or_like('patient_patient_last_name', $search_data);
		$this->db->limit(15);

		$query = $this->db->get($this->table_name);
		$result = $query->result();

		return $result;

		
	}

	public function display_patient($id) {
		
		$this->db->from($this->table_name);
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();	// This returns one row only

	}

}

?>
