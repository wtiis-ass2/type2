<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Patient extends CI_Controller {


	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('patient_model');
		$this->load->model('record_model');

		// $this->output->enable_profiler(TRUE);  // Just for debugging stuffs
	}

	public function index($patient_id) {

		$patient = $this->patient_model->display_patient($patient_id);
		$records = $this->record_model->get_records($patient_id);
		$template = $this->record_model->get_insert_template();
		$latest = $this->record_model->get_latest_record($patient_id);

		$data['latest'] = $latest;
		$data['ugly'] = $template[1];	// ugly template 
		$data['patient'] = $patient;
		$data['template'] = $template[0]; // use the pretty version of the template
		$data['title'] = "Patient records";
		$data['records'] = $records;

		$this->load->view('templates/header', $data);
		$this->load->view('patient_page', $data);
		// patient_page.php is using its own footer inside the doc
		// $this->load->view('templates/footer');

		
		
	}

// ---------------------------------------------------------------------

//						Helper Functions Below

// ---------------------------------------------------------------------

	public function create_record() {
		
		$template = $this->record_model->get_insert_template();

		foreach ($template[1] as $column){	// use the original format of the column template
			$data[$column] = $this->input->post($column);
		}

		$insert = $this->record_model->add_record($data);

		echo json_encode(array("status" => TRUE));

	}


	public function generate_waist($patient_id) {

		$attribute = 'waist_circumfrerence';
		
		$waistData = $this->record_model->get_required_chart($patient_id, $attribute);

		echo json_encode($waistData);

	}

	public function generate_glucose($patient_id) {
		
		$attribute = 'blood_glucose_level';

		$glucose = $this->record_model->get_required_chart($patient_id, $attribute);

		echo json_encode($glucose);

	}


}

?>

