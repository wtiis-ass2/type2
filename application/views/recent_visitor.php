<div class="mt-5">
    <table class="table table-hover">
        <thead>
        <tr style="text-align:center">
            <th scope="col">Patient ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Last Visit Date</th>
            <th scope="col"></th>

        </tr>
        </thead>
        <tbody>
        <?php
        if (isset($recent)){
            foreach ($recent as $row){ 

                $prettyDate = date("D \, d F Y", strtotime($row['last_vist_date']));

                ?>

                <tr class="table-light" style="text-align:center">
                        <th scope="row">
                            <?= $row['id'];?>
                        </th>
                        
                        <td><?= $row['first_name'] ?></td>
                        <td><?= $row['last_name'] ?></td>
                        <td><?= $prettyDate ?></td>
                        <td><a href="<?= base_url(); ?>patient/<?= $row['id']?>">visit</a></td>

                </tr>

            <?php
            }
        }
        ?>
        </tbody>
    </table>
</div>


