<!--
NOT USING CHEADER AT THE MOMENT, SUBJECT TO DELETE IF THERE IS NO PROBLEM WITH CALENDAR ITEM
 -->

<html>
<head>


    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.0/flatly/bootstrap.min.css" rel="stylesheet" integrity="sha384-kCsv8pSAWtRge/+zcLDeqwoWhTQSUX2esQPYOsocgrg1eMj7T2wrTJP348T3mpBU" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/awesomestuff.css" />



    <title><?= $title ?></title>

</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
    <div class="container">

        <a class="navbar-brand" href="<?= base_url(); ?>dietitian/homepage">
            <i class="fab fa-medrt fa-2x"></i>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor03">
            <ul class="navbar-nav mr-auto" id="navMenu">


                <?php $url=base_url() . "dietitian" . "/";

                $homepage = $url . "homepage";
                $recentvisit = $url . "recentvist";
                $education = $url . "education";
                $calendar = $url . "calendar"

                ?>

                <li class="nav-item <?php if (current_url() == $homepage) {echo "active";}  ?>">
                    <a class="nav-link" href="<?= base_url(); ?>dietitian/homepage">Home<span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item <?php if (current_url() == $recentvisit) {echo "active";}  ?>">
                    <a class="nav-link" href="<?= base_url(); ?>dietitian/recentvist">Recently-Visited</a>
                </li>

                <li class="nav-item <?php if (current_url() == $education) {echo "active";}  ?>">
                    <a class="nav-link" href="<?= base_url(); ?>dietitian/education">Education-Materials</a>
                </li>

                <li class="nav-item <?php if (current_url() == $calendar) {echo "active";}  ?>">
                    <a class="nav-link" href="<?= base_url(); ?>dietitian/calendar">Calendar</a>
                </li>

            </ul>

            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-md fa-lg"></i></a>
                    <!-- <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);"> -->
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="<?= base_url(); ?>dietitian/account_settings">Account settings</a>
                        <a class="dropdown-item" href="<?= base_url(); ?>dietitian/help">Help</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= base_url(); ?>">Sign out</a>       <!-- need to clearsession as well in here -->
                    </div>
                </li>
            </ul>


        </div><!-- closing div class navbar collapse -->
    </div><!-- closing div container class -->
</nav>

<!-- <br><br><br><br><br> -->
<div class="container">


