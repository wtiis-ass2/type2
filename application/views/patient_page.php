
<div class="jumbotron mt-5">
	<div class="d-flex flex-wrap" >
		<div class="mr-auto p-3">
			
			<?php 
				$first_name = $patient->patient_first_name;
				$last_name = $patient->patient_patient_last_name;
				$full_name = $first_name . " " . $last_name;
			?>

			<h2><?= $full_name ?></h2>
			<table>
				<tbody>
					<tr>
						<td>DOB</td>
						<td>:</td>
						<td><?= date("d F Y", strtotime($patient->patient_dob));?></td>
					</tr>
					<tr>
						<td>Gender</td>
						<td>:</td>
						<td><?= $patient->patient_gender ?></td>
					</tr>
					<tr>
						<td>Email</td>
						<td>:</td>
						<td>
						<a href="mailto:<?= $patient->patient_email ?>"><?= $patient->patient_email ?></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="p-3" >
			<canvas id="bloodGlucose"></canvas>
		</div>
		<div class="p-3">
			<canvas id="waist"></canvas>
		</div>
	</div>

</div>

<div class="mt-5 mb-3 d-flex justify-content-between">
	<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#new_record" id="newRecordControl">+ new record</button>
	
	<?php 
		if (!empty($records)){ ?>

		<div class="d-flex flex-row">
			<button type="button" class="btn btn-sm btn-outline-info openAll mr-2" >Expand all</button>
			<button type="button" class="btn btn-sm btn-outline-info closeAll" >Hide all</button>
		</div>
	
	<?php
	}
	?>
</div>


<!------------------------------------------------------------------------------------------------>

<!-- 										Create new record 									-->

<!------------------------------------------------------------------------------------------------>
<?php 
	
	// if (!empty($latest)){
	// 	$latestRecordDate = ($latest[0]->record_modified_date);
	// }

	// else{
	// 	echo "nothing to see";
	// }
	// echo $patient->id;
	// echo $debug;
?>

<div id="accordion">
	<div id="new_record" class="collapse">
	  <div class="card mb-3 border-success" style="cursor: pointer;">
	    <div class="card-header" id="newRecord">
	      <h5 class="mb-0">
			 <h4 class="text-success" data-toggle="collapse" href="#collapseNew" aria-expanded="true" aria-controls="collapseNew">
	          <?=  date("D \, d F Y");?>
	        </h4>
	      </h5>
	    </div>

	    <div id="collapseNew" class="collapse show" aria-labelledby="newRecord" data-parent="#accordion">
	      <div class="card-body">

	      	<form class="w-60" action="#" id="newRecordForm" name="newRecordForm">

	      		<!-- should also add a hidden input to verify session, in the future -->

				<input type="hidden" name="patient_id" value="<?= $patient->id ;?>">

				<input type="hidden" name="record_created_date" value="<?= date("Y/m/d") ?>">

				<input type="hidden" name="record_modified_date" value="<?= date("Y/m/d") ?>">

				<input type="hidden" name="validity_flag" value="1">

				<?php 

				for ($i=4; $i < sizeOf($template) ; $i++) { ?>

					  <div class="form-group row">
					    <label for="<?= $template[$i]?>" class="col-sm-2 col-form-label"><?= $template[$i]?></label>
					    <div class="col-sm-10">
					      <input type="text" placeholder="<?=$template[$i]?>" class="form-control" id="<?= $ugly[$i]?>" name="<?= $ugly[$i]?>">
					    </div>
					  </div>

	  				  <?php 
		      		}

		      	?>

		      	<div class="d-flex justify-content-center">
	               <button type="button" id="btnSave" onclick="create_record()" class="btn btn-outline-success mt-3 mr-2">Save</button>
	               <button type="button" id="btnSave" onclick="reset_new_record()" class="btn btn-outline-warning mt-3 ml-2">Reset</button>
				</div>

			</form>

	      </div>
	    </div>
	  </div>
	</div>
</div>

<!---------------------------------------->
<!-- 				End 			 	-->
<!---------------------------------------->



<?php 

	if (!empty($records)){

		$index = 0;
		?>
		<div id="accordion">
		<?php 
		foreach ($records as $record) {

			$details = array();

			foreach ($record as $item){
				array_push($details, $item);
			}

			
			$create_date = date("D \, d F Y", strtotime($details[2]));

			?>
			
  
			  <div class="card mb-3" style="cursor: pointer;">
			    <div class="card-header" id="heading<?=$index?>">
			      <h5 class="mb-0">
			        <h4 data-toggle="collapse" href="#collapse<?=$index?>" aria-expanded="true" aria-controls="collapse<?=$index?>">
			          <?= $create_date?>
			        </h4>
			      </h5>
			    </div>
			    
			    <div id="collapse<?=$index?>" class="collapse comboCollapse" aria-labelledby="heading<?=$index?>"> <!-- data-parent="#accordion"-->
			      <div class="card-body">

			      	<table class="table table-hover w-50">
			      		<tbody>
			      			<?php 

					      		for ($i=4; $i < sizeOf($details)-1 ; $i++) { ?>
					      			
					      			<tr>
					      				<td><?= $template[$i]?></td>
					      				<td><?= $details[$i]?></td>
					      			</tr>

					      			<?php 
					      		}

					      	?>
			      		</tbody>
			      	</table>

			      </div>
			    </div>
			  </div>

			<?php

			$index += 1;
		} // closing for loop
		?>
		</div> <!-- closing accordion -->
		<?php
	}

?>


</div>	<!-- closing container -->
</div> <!-- closing content -->
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script type="text/javascript">
// ---------------------------------------------------------------------
// 					 	change text after clicking
// ---------------------------------------------------------------------

function change_text_when_toggled(){
	this.textContent = (this.textContent == '+ new record' ? 'cancel' : '+ new record');
}
// console.log(document.getElementById('newRecordControl').textContent);

document.getElementById('newRecordControl').onclick=change_text_when_toggled;

$('.closeAll').click(function(){
  $('.comboCollapse')
    .collapse('hide');
});

$('.openAll').click(function(){
  $('.comboCollapse')
    .collapse('show');
});


function reset_new_record() {
	document.getElementById("newRecordForm").reset();
}

function create_record() {
	var url;
	var blob;
	
	url = "<?= base_url(); ?>patient/create_record";
	
	blob = $('#newRecordForm').serialize();
	console.log(blob);	
	
	// ajax adding data to database
	$.ajax ({
		url : url,
		type: "POST",
		data: $('#newRecordForm').serialize(),
		dataType: "JSON",
		success: function(data) {
			//if success close modal and reload ajax table
			$('#collapseNew').collapse('hide');
			alert('Record was added');
			location.reload();// to reload a page
		},

		error: function (jqXHR, textStatus, errorThrown,json) {
			$('#collapseNew').collapse('hide');
			alert('Error adding new record');
		}
	});

}


// ---------------------------------------------------------------------
// 					 	Generate waist chart
// ---------------------------------------------------------------------

$(document).ready(function(){
	var url = "<?php echo base_url(); ?>patient/generate_waist/<?= $patient->id; ?>";
	console.log(url);
	$.ajax({
		url: url,
		method: "GET",
		success: function(data) {
			console.log(data);
			var helper_visit = [];
			var helper_waist = [];

			var visit = [];
			var waist = [];

			var parsed_data = (JSON.parse(data));

			for(var i in parsed_data) {
				helper_visit.push(parsed_data[i].record_created_date);
				helper_waist.push(parsed_data[i].waist_circumfrerence);
			}

			for(var i in parsed_data) {
				visit.push(helper_visit.pop());
				waist.push(helper_waist.pop());
			}

			var chartdata = {
				labels: visit,
				datasets : [
					{
						label: 'Waist Circumference',
            			fill: false,
            			backgroundColor: 'rgb(0, 99, 132)', // this is the fill of line colour
            			borderColor: 'rgb(255, 99, 132)',		// border of line
						data: waist
					}
				]
			};

			var options = {
		    	legend:{
		        	display: false,
		        },
		        title: {
		            display: true,
		            text: 'Waist Circumference'
	        	}
		    };

			var ctx = document.getElementById('waist').getContext('2d');

			var chart = new Chart(ctx, {
				type: 'line',
				data: chartdata,
				options: options
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
});

// ---------------------------------------------------------------------
// 					 	Generate glucose chart
// ---------------------------------------------------------------------

$(document).ready(function(){
	
	var url = "<?php echo base_url(); ?>patient/generate_glucose/<?= $patient->id; ?>";
	console.log(url);
	$.ajax({
		url: url,
		method: "GET",
		success: function(data) {
			console.log(data);
			var helper_visit = [];
			var helper_gluc = [];

			var visit = [];
			var gluc = [];

			var parsed_data = (JSON.parse(data));

			for(var i in parsed_data) {
				helper_visit.push(parsed_data[i].record_created_date);
				helper_gluc.push(parsed_data[i].blood_glucose_level);
			}

			for(var i in parsed_data) {
				visit.push(helper_visit.pop());
				gluc.push(helper_gluc.pop());
			}

			var chartdata = {
				labels: visit,
				datasets : [
					{
						label: 'Blood Glucose Level',
            			fill: false,
						backgroundColor: 'rgba(46, 217, 118, 0.5)',
						data: gluc
					}
				]
			};

			var options = {
		    	legend:{
		        	display: false,
		        },
		        title: {
		            display: true,
		            text: 'Blood Glucose Level'
	        	}
		    };

			var ctx = document.getElementById('bloodGlucose').getContext('2d');

			var chart = new Chart(ctx, {
				type: 'bar',
				data: chartdata,
				options: options
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
});
</script>

