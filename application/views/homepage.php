
<div class="container h-100">
  <div class="row h-100 justify-content-center align-items-center">
    <div class="col-7">
     

     <form class="my-2 my-lg-0">
      <div class="d-flex justify-content-between">
        <label><h1 class="h3 mb-3 font-weight-normal">Patient Search</h1></label>
        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#new_patient">+ new patient</button>
      </div>
          
          <input class="form-control mr-sm-10" for="search_data" id="search_data" name="search_data" type="text" autocomplete="off" placeholder="Type Patient's Last Name ..."  onkeyup="search_patient()" onfocus="search_patient()">
            
            <div id="suggestions" >
              <div id="patient_suggestion"></div>
            </div>

          <br>
          <p class="text-center" id="quote">
            "In nothing do men more nearly approach the gods than in giving health to men." 
            </p>
            <p class="text-right" id="quote"> - Cicero (106 B.C. - 43 B.C.)</p>
            <!-- <input type="submit" class="btn btn-primary btn-sm" name="submit" value="Search"  /> -->
          
     </form>

      <div class="modal fade" id="new_patient">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title">Add new patient</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

                <form action="#" id="form" name="form">
                  <fieldset>

                    <div class="form-group">
                      <label for="users_id">Dietitian ID</label>
                      <input type="text" class="form-control" id="users_id" aria-describedby="emailHelp" name="users_id" placeholder="Enter your dietitian id">
                    </div>

                    <div class="form-group">
                      <label for="patient_first_name" >Patient's First Name</label>
                      <input type="text" class="form-control" id="patient_first_name" aria-describedby="emailHelp" name="patient_first_name" placeholder="Enter patient's first name">
                    </div>

                    <div class="form-group">
                      <label for="patient_last_name" >Patient's Last Name</label>
                      <input type="text" class="form-control" id="patient_last_name" aria-describedby="emailHelp" name="patient_last_name" placeholder="Enter patient's last name">
                    </div>

                    <fieldset class="form-group">
                      <legend for="patient_gender"><h6>Patient's Gender</h6></legend>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="patient_gender" id="optionsRadios1" value="2" checked="">
                        Male
                      </label>
                    </div>

                    <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="patient_gender" id="optionsRadios2" value="1">
                        Female
                      </label>
                    </div>

                    <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="patient_gender" id="optionsRadios3" value="3">
                        Neutral
                      </label>
                    </div>

                    </fieldset>


                    <div class="form-group">
                      <label for="patient_dob" >Patient's DOB</label>
                      <input type="text" class="form-control" id="patient_dob" aria-describedby="emailHelp" name="patient_dob" placeholder="Enter patient's dob">
                    </div>

                    <div class="form-group">
                      <label for="patient_email" >Patient's Email</label>
                      <input type="text" class="form-control" id="patient_email" aria-describedby="emailHelp" name="patient_email" placeholder="Enter patient's email">
                    </div>
                    
                    <button type="button" id="btnSave" onclick="create_patient()" class="btn btn-primary">Save</button>

                  </fieldset>

                </form>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>