-- create database
-- Database: `type2`
DROP DATABASE IF EXISTS type2;
CREATE DATABASE type2;


-- drop tables if already exists
DROP TABLE IF EXISTS type2.booking;
DROP TABLE IF EXISTS type2.patient_record;
DROP TABLE IF EXISTS type2.patient;
DROP TABLE IF EXISTS type2.dietitian;
DROP TABLE IF EXISTS type2.time_slot;
DROP TABLE IF EXISTS type2.admin;


-- create tables



CREATE TABLE type2.groups (
  id mediumint(8) UNSIGNED NOT NULL,
  name varchar(20) NOT NULL,
  description varchar(100) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'guest', 'guest viewing only');


CREATE TABLE type2.login_attempts (
  id int UNSIGNED NOT NULL,
  ip_address varchar(15) NOT NULL,
  login varchar(100) NOT NULL,
  `time` int UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE type2.time_slot (
    id int NOT NULL AUTO_INCREMENT,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE type2.users (
  id int UNSIGNED NOT NULL AUTO_INCREMENT,
  ip_address varchar(45) NOT NULL,
  username varchar(100) DEFAULT NULL,
  password varchar(255) NOT NULL,
  salt varchar(255) DEFAULT NULL,
  email varchar(100) NOT NULL,
  activation_code varchar(40) DEFAULT NULL,
  forgotten_password_code varchar(40) DEFAULT NULL,
  forgotten_password_time int(11) UNSIGNED DEFAULT NULL,
  remember_code varchar(40) DEFAULT NULL,
  created_on int(11) UNSIGNED NOT NULL,
  last_login int(11) UNSIGNED DEFAULT NULL,
  active tinyint(1) UNSIGNED DEFAULT NULL,
  first_name varchar(50) DEFAULT NULL,
  last_name varchar(50) DEFAULT NULL,
  company varchar(100) DEFAULT NULL,
  phone varchar(20) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE type2.patient (
    id int UNSIGNED NOT NULL AUTO_INCREMENT,
    users_id int UNSIGNED NOT NULL,
    patient_first_name varchar(255),
    patient_patient_last_name varchar(255),
    patient_gender ENUM('F','M','neuter') NOT NULL,
    patient_dob DATE NOT NULL,
    patient_email varchar(255),
    PRIMARY KEY (id),
    FOREIGN KEY (users_id) REFERENCES users(id)
);

CREATE TABLE type2.patient_record (
    record_id int NOT NULL AUTO_INCREMENT,
    patient_id int UNSIGNED NOT NULL,
    record_created_date DATE NOT NULL,
    record_modified_date DATE NOT NULL,
	blood_pressure int,
	blood_glucose_level decimal,
	waist_circumfrerence decimal,
	weight decimal,
	height decimal,
	validity_flag bit NOT NULL,
    PRIMARY KEY (record_id),
    FOREIGN KEY (patient_id) REFERENCES patient(id)
);


CREATE TABLE type2.booking (
    booking_id int NOT NULL AUTO_INCREMENT,
    dietitian_id int UNSIGNED NOT NULL,
    patient_id int UNSIGNED NOT NULL,
    slot_id int,
    booking_created_date DATE NOT NULL,
    booking_modified_date DATE NOT NULL,
	booking_date DATE NOT NULL,
	validity_flag bit NOT NULL,
    PRIMARY KEY (booking_id),
    FOREIGN KEY (dietitian_id) REFERENCES users(id),
    FOREIGN KEY (patient_id) REFERENCES patient(id),
    FOREIGN KEY (slot_id) REFERENCES time_slot(id)
);

INSERT INTO type2.time_slot (start_time, end_time)
VALUES
("08:00", "10:00"),
("10:00", "12:00"),
("12:00", "14:00"),
("14:00", "16:00");


INSERT INTO `users` (`ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
('127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', 'ZzvdPB4jEJ4bufboe9Bczee0fd6554f25703da67', 1525153827, NULL, 1268889823, 1525914259, 1, 'Admin', 'istrator', 'ADMIN', '0'),
('::1', NULL, '$2y$08$xJ9my386D4QrAFpd4oJx7Obtg7zhqSXNUBHIE4bKg93mKHuGaHsRq', NULL, 'didi@blob.com', NULL, NULL, NULL, NULL, 1525153464, 1525154382, 1, 'didi', 'blob', 'nothing', '111'),
('::1', NULL, '$2y$08$pmJKRPkaVznkY8biBw/ySuqXMJswKs03cU0x0gYGUgrEWAQKxC0am', NULL, 'harry.w@wsu.edu.au', NULL, NULL, NULL, NULL, 1525913789, NULL, 1, 'Harry', 'Wang', 'WSU', '');

CREATE TABLE `users_groups` (
  id int UNSIGNED NOT NULL AUTO_INCREMENT,
  user_id int UNSIGNED NOT NULL,
  group_id mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(8, 2, 1),
(9, 2, 2),
(10, 3, 2);
--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  -- ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;


--
--
-- create views
--

CREATE VIEW recent_visit_id AS
    SELECT patient_id, max(record_created_date) as last_vist_date
    FROM patient_record
    GROUP BY patient_id
    limit 10;

CREATE VIEW recent10 AS
    select patient_id as id, patient_first_name as first_name, patient_patient_last_name as last_name, last_vist_date
    from recent_visit_id as T, patient
    where t.patient_id=patient.id;


CREATE VIEW booking_event AS
    SELECT CONCAT(p.patient_first_name, ' ', p.patient_patient_last_name) As title, b.booking_id as id, CONCAT(b.booking_date, 'T',t.start_time) As start, CONCAT(b.booking_date, 'T',t.end_time) As end
    From booking as b, patient as p, time_slot as t
    Where b.patient_id = p.id AND b.slot_id=t.id;


