


<!-- <footer class="footer">
  <div class="container">
  	<center>
    	<span class="text-muted" >&copy; 2018 Type 2</span>
    </center>
  </div>
</footer> -->


</div>	<!-- closing container -->
</div> <!-- closing content -->
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script type="text/javascript">
	// $('form').attr('autocomplete', 'off'); // disable autocomplete on forms

	function create_patient() {
		var url;
		var blob;
		
		url = "<?= base_url(); ?>dietitian/create_patient";
		
		blob = $('#form').serialize();
		// ajax adding data to database
		$.ajax ({
			url : url,
			type: "POST",
			data: $('#form').serialize(),
			dataType: "JSON",
			success: function(data) {
				//if success close modal and reload ajax table
				$('#new_patient').modal('hide');
				alert('Patient added successfully');
				location.reload();// to reload a page
			},
			error: function (jqXHR, textStatus, errorThrown,json) {
				$('#new_patient').modal('hide');
				alert('Error adding new patient');
			}
		});

	}



	function search_patient() {

		var user_input = $('#search_data').val();

		$(document).ready(function(){

		    $("#search_data").focusout(function(){
		        setTimeout(function(){
			       $('#suggestions').hide();		// hide the patient suggestion list
			    }, 150);
		    });

		});

		if (user_input.length === 0)  { 		// if no input from the user

			$('#suggestions').hide();		// hide the patient suggestion list

		}

		else {	// if user inputs something to the search box

			var url = "<?php echo base_url(); ?>dietitian/search";

			var post_data = {
				'search_data': user_input,
				'<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
			};

			$.ajax({
				type: "POST",
				url: url,
				data: post_data,
				success: function(data) {
					if (data.length > 0) {
						$('#suggestions').show();
						$('#patient_suggestion').addClass('patient_list');
						$('#patient_suggestion').html(data);
					}
				},
				error: function (jqXHR, textStatus, errorThrown,json) {
					console.log("you screwed up");
				}

			}); // closing ajax 

		} // closing else statement

	}


</script>
</body>
</html>
