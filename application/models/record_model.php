<?php

defined('BASEPATH') OR exit('No direct script access allowed');
//This is the Book Model for CodeIgniter CRUD using Ajax Application.


class record_model extends CI_Model {

	public $table_name='patient_record';

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_insert_template() {

		$result = $this->db->list_fields($this->table_name);

		foreach ($result as $item) {

			$pretty = str_replace('_', ' ', ucfirst($item));
			// $awesome = str_replace('_', ' ', ucfirst($item))
			$out[] = $pretty; 	// appending item into the $out array 
			$ori[] = $item;

		}

		// $final = array_shift($out);
		unset($out[0]);
		unset($ori[0]);

		$final[0] = $out;
		$final[1] = $ori;
		// return $out;	// This returns an array of rows
		return $final;
	}


	public function get_records($patient_id) {
		
		$this->db->from($this->table_name);
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by("record_modified_date", "desc");
		$query = $this->db->get();

		return $query->result();

	}

	public function get_latest_record($patient_id) {
		
		$this->db->from($this->table_name);
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by("record_modified_date", "desc");
		$this->db->limit(1);
		$query = $this->db->get();

		return $query->result();

	}


	public function add_record($data) {
		
		$this->db->insert($this->table_name,$data);
		return $this->db->insert_id();

	}


	public function get_required_chart($patient_id, $attr) {
		
		$this->db->select($attr.', record_created_date');
		$this->db->from($this->table_name);
		$this->db->where('patient_id', $patient_id);
		$this->db->order_by("record_id", "desc");
		$this->db->limit(7);
		$query = $this->db->get();

		return $query->result();

	}

}

?>
